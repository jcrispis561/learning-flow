Descripción:

Necesitamos una version del dataframe que pueda interactuar directamente con el plugin de MongoDB y sus funciónes map reduce, esas funciones van a tomar las funciones de pandas y aplicarlas a los datos en el middleware, ademas de eso necesita trabajar con nuestra versión especial de los archivos, que tendran un funcionamiento parecido, aplicando funciónes de transformación en el middleware y pasandolos a pandas, este dataframe necesita abrir los archivos de una manera distinta, como cadenas de texto, si no se le indican los headers tampoco parsearlos, todas esas funciones estaran en el API del archivo, tambien necesita poder aplicar funciones sobre el mismo usando multiprocesamiento, para eso debemos tener un dataframe por core.

Por ejemplo si se especifican n workers tener n cantidad de dataframes y que cada uno contenga una parte de la información del archivo, tanto el nuevo archivo como el plugin de mongo tendran metodos para mandar una cantidad de los archivos. 

Requerimientos:
- [ ] Una clase dataframe modificada
- [ ] Integrar las operaciónes de los middlewares a la version modificada del dataframe
- [ ] Tener un dataframe manager para multiprocesamiento que cree varias versiones del dataframe y divida los datos en la cantidad de nucleos.
- [ ] Reemplazar la funcion de indexación y slicing del nuevo dataframe para que se adapte al funcionamiento del nuevo workflow
- [ ] Hacer metodos que puedan devolver los datos de los dataframe en forma de matrices numpy de forma paralela.
