Descripción:

Tomar los modelos de scikit-learn y aplicar funciones de promedio para los modelos regresivos y consenso para los modelos de clasificación y arboles, ademas de poder hacer un Wrapper para hacer ensemble learning aprovechando el multiprocesamiento. Modificar la funcion fit del modelo base de Sklearn para que se puedan comunicar con Pandas y entrenar 1 modelo distinto por nucleo, la cantidad de modelos que se generaran van a ser igual a la cantidad de workers que se especifiquen.

Requerimientos:
- [ ] Clase Wrapper para los modelos regresivos.
- [ ] Clase Wrapper para los modelos predictivos.
- [ ] Clase Wrapper para los preprocesadores.
- [ ] Algoritmos de consenso para los clasificadores.
- [ ] Metodos de predicción para los modelos regresivos.
- [ ] Aplicar funciones logisticas y binarias para convertir modelos regresivos en modelos de clasificación.
