Descripción:

Nuesto plugin de database necesita poder tomar un dataframe y guardarlo en una base de datos MongoDB, ademas de aplicar metodos de Map reduce comunes de datascience de manera temporal, a su vez poder tener funciones para recuperar el nombre de las columnas, por ahora trabajar con un archivo de configuración a parte, no guardes datos privados en ese archivo, usa __un runtime de mongo localmente__, nunca modificar los datos directamente con ni una de las operaciones de mapeo, todas esas opraciones deben funcionar del punto de vista de middleware, asi podemos ahorrar RAM y no usar pandas para preprocesar los datos si no solo para mostrar resultados, las funciones map reduce no debes encerrarlas dentro de un objeto, __para testear el comportamiento usa datasets publicos__

Requerimientos:

- [ ] Archivo de configuración que acepte todos los parametros del cliente de PyMongo (visitar la documentación)
- [ ] Respetar las resposabilidades de las clases, 1 clase 1 resposabilidad, la clase de conexión solo se debe encargar de eso.
- [ ] Nombres representativos y respetar el estandar PEP8 con el linter pycodestyle.
- [ ] Una clase que pueda cargar los datos desde la base de datos a un dataframe.
- [ ] Un metodo de clase que pueda pasar los datos desde el dataframe directo a mongoDB.
- [ ] Funciones de map reduce como middleware.
- [ ] Tests coverage minimo 80%.
- [ ] Guardar los resultados del perfilado en un archivo a parte.

Luego de terminar todos los requerimientos puedes hacer la documentación y marcar el modulo como completado, sugerencias y mejoras es algo que se debe discutir, pero siempre se esta abierto a ello en fin de ir un paso adelante al exito