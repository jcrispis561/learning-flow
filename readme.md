# La idea principal

Este proyecto se basa en paralelizar el workflow de machine learning como primera meta, y segunda meta es optimizar la RAM esa es la fase alpha de este proyecto, actualmente los modelos de machine learning se pueden paralelizar a escala empresarial, ya que es un proceso engorroso y muy largo paralelizar solo un modelo, es algo que se sabe hacer de hace mucho tiempo. Con el auge de los bootcamps de Datascience cada vez menos gente sabe paralelizar o usar multiprocesamiento en sus modelos, de ahi nace la necesidad de esta herramienta, tomando librerias como __Pandas__ y __Scikit Learn__ para dividir los trabajos en varios procesos que se ejecuten en nucleos separados.

Al ser un proceso común la viabilidad del proyecto es total, pero me gustaria para la Beta ir mucho mas alla e investigar otras maneras de optimización para mejorar los resultados de los modelos predictivos.

# ¿Como contribuir?

Puedes hacer fork de este repositorio, instalar los requerimientos y el entorno virtual con los siguiente comando

~~~sh
    $ conda env create -f environment.yml
~~~

~~~sh
    $ conda activate mlflow
    $ pip install -r requirements.txt
~~~

Requerimientos para el desarrollo:
    <p>- [Anaconda](https://www.anaconda.com/products/individual) ó [Miniconda](https://docs.conda.io/en/latest/miniconda.html)</p>
    <p>- [MongoDB](https://www.mongodb.com/community) Advertencia: Siempre usar Mongo en modo local durante el desarrolo </p>

seguir las siguientes pautas de trabajo:

- Los test van primero, para los test usamos el framework pytest que esta especificado en los requirementos, un modulo sin los tests no seran aceptados.

- Todas las pautas de los modulos y los requerimientos individuales estan en sus respectivas carpetas, cada uno debe reportar en que modulo esta trabajando durante esta fase, ya que la asignación de trabajos es un modulo por persona.

- Luego de terminar una función debes hacer un profiling con line-profiler para detectar cuellos de botella en el CPU y posible, siempre antes de refactorizar el codigo perfilalo antes, o si no no tiene sentido refactorizar.

- Esto es solo por diversión :D.